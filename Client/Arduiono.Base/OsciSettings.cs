using System;
namespace Arduiono.Osci.Base
{
    public class OsciSettings
    {
        public string SerialPort
        {
            get;
            set;
        }

        public long ChannelCount
        {
            get;
            set;
        }
    }
}